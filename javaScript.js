let btngetGrade = document.querySelector('#sendGrade')
let showGrades = document.querySelector('#outGreades')
let inputGrade = document.querySelector('.inputGrade')
let labelMean = document.querySelector('#labelMean')
let showMean = document.querySelector('#btnMean')
let tagP = document.createElement('p')
let grades = []
let count = 0
let mean = 0

//adição de notas as lista
btngetGrade.addEventListener('click', () => {    
    // verificação de entrada
    let test = inputGrade.value.replace(",",".")
    let grade = parseFloat(test)    
    // adição de notas as lista e atribuição do valores em vetor
    if(inputGrade.value != ""){
        if(grade >= 0 && grade <=10 ){                        
            count++
            grades.push(grade)            
            showGrades.innerHTML += "Nota "+count+": "+ grade +"\n" 
            inputGrade.value = ""           
        }
    // Casos sem validade
        else{
            alert('A nota digitada é inválida, por favor, insira uma nota válida.')
        }
    }
    else{
        alert("Defina um valor para nota no campo 'Nota'")
    }        
})

// Calculo de média
showMean.addEventListener('click', () => {
    
    if(grades.length != 0){
        let sum = 0 
        // Calculo de média   
        sum = grades.reduce((accumulator, current) => accumulator + current)
        mean = sum/count
        // console.log
        mean = mean.toFixed(2)         
        // atribuição do valor da média para elemento html     
        labelMean.innerHTML = mean 
        // ativiação da vilicilidade do elemento que apresenta a média
        labelMean.removeAttribute('hidden')
    }
    // Caso sem validade
    else{
        alert("Insira as notas primeiro para calcumlar a média.")
    }
})